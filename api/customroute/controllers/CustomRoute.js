'use strict';

/**
 * Read the documentation () to implement custom controller functions
 */

module.exports = {
    redirect: async ctx => {
        ctx.redirect('/gdidashboard');
    }
};
